#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Just open HTTPserver from shots directory"""
import os
from http.server import HTTPServer, SimpleHTTPRequestHandler

os.chdir("/home/pi/shots")
HTTP = HTTPServer(('0.0.0.0', 80), SimpleHTTPRequestHandler)
HTTP.serve_forever()
