#!/usr/bin/python3
# -*- coding: utf-8 -*-
# sudo apt-get install libsdl1.2-dev libsdl-image1.2-dev
#                      libsdl-mixer1.2-dev libsdl-ttf2.0-dev
"""Raspberry module for taking pictures"""
import os
import sys
import time
import logging
from datetime import datetime
import pygame
import pygame.camera
import RPi.GPIO as RPIO

CONFIG = {
    "dim": (640, 480),
    "device": "/dev/video0",
    "pin": 20,
    "led": 24
}

os.environ["SDL_VIDEODRIVER"] = "dummy"


class Clicker():
    """Picture taking class."""
    def __init__(self):
        self.log = logging.getLogger(__name__)
        logging.basicConfig(level=logging.INFO)
        self.log.info("Script started")
        pygame.init()
        self.log.info("Library initialized")
        pygame.camera.init()
        self._cam = pygame.camera.Camera(CONFIG["device"], CONFIG["dim"])
        self.log.info("Camera opened")
        self.filename = None
        self._led = None
        try:
            self._cam.start()
            self.log.info("Camera started")
        except SystemError as err:
            self._cam = None
            self.log.info("Camera start failure: %s", format(err))
            raise

        RPIO.setmode(RPIO.BCM)
        RPIO.setup(CONFIG["pin"], RPIO.IN, pull_up_down=RPIO.PUD_UP)
        if CONFIG["led"] is not None:
            RPIO.setup(CONFIG["led"], RPIO.OUT)

    def _shot(self, name='picture.jpg'):
        """Shot a picture and save it under "name" """
        self.log.info("Shooting %s", name)
        self.switch_led(True)
        image = self._cam.get_image()
        pygame.image.save(image, name)
        self.log.info("Image saved: %s", name)
        self.switch_led(False)

    def switch_led(self, state=True):
        """swith led to state"""
        self._led = state
        if CONFIG["led"] is not None:
            RPIO.output(CONFIG["led"], state)

    def capture(self):
        """Wait for GPIO event and capture a shot"""
        RPIO.wait_for_edge(CONFIG["pin"], RPIO.FALLING)
        # https://docs.python.org/3/library/datetime.html
        self.filename = "/home/pi/shots/Shot %s.jpg"
        self.filename %= datetime.now().strftime("%Y%m%d%H%M%S")
        self._shot(self.filename)
        time.sleep(10)

    def __del__(self):
        if self._cam is not None:
            self._cam.stop()
            self.log.info("Camera stopped")
        pygame.quit()
        RPIO.cleanup()
        self.log.info("Script exit")


if __name__ == "__main__":
    try:
        CLICKER = Clicker()
    except SystemError as err:
        sys.exit()
    while True:
        CLICKER.capture()
