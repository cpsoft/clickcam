# clickcam.py
Главный скрипт. Запускается, активирует камеру и ждет нажатий кнопки. После каждого нажатия кнопки, делает снимок и сохраняет его, с названием - текущей датой.
## Установка
Перед используем требуется:
 - Включить камеру в raspi-config 
 - Выполнить в командной строке
```
  sudo apt-get -qq update
  sudo modprobe bcm2835-v4l2
  sudo apt-get -qq install git python3 python3-pip python3-pygame libsdl1.2-dev libsdl-image1.2 libbluetooth-dev
  pip3 install --upgrade pip
  git clone https://gitlab.com/cpsoft/clickcam.git
  mkdir shots
  cd clickcam
  sudo python3 -m pip install -r requirements.txt 
```

## Настройка
см. блок CONFIG в файле `clickcam.py`:
```
CONFIG = {
    "dim": (640, 480), # Разрешение снимка
    "device": "/dev/video0", # Устройство, к которому подключена камера
    "pin": 3 # Pin, к которому подключена кнопка
    "led": 24 # Pin, к которому подключен светодиод вспышки
}
```
см. [описание подключения](https://gitlab.com/cpsoft/clickcam/wikis/%D0%9F%D0%BE%D0%B4%D0%BA%D0%BB%D1%8E%D1%87%D0%B5%D0%BD%D0%B8%D0%B5-Raspberry-PI-%D0%B2-Headless-%D1%80%D0%B5%D0%B6%D0%B8%D0%BC%D0%B5#%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-%D1%81%D0%B2%D1%8F%D0%B7%D0%B8-bluetooth-%D0%B2%D0%B0%D1%80%D0%B8%D0%B0%D0%BD%D1%82-1-%D0%BE%D0%BF%D1%82%D0%B8%D0%BC%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9):

`testbt.py` - настройки не требуются.

# server.py
Сервер доступа к файлам через браузер.
Запуск:
```
sudo python3 server.py
```
Открываем браузер по адресу малинки *(как найти??)* и видим сделанные фотки. После появления новых фоток обновить страницу.

# Полезные ссылки
* http://dmitrysnotes.ru/obzor-odnoplatnogo-kompyutera-raspberry-pi-zero-w
* https://learn.adafruit.com/turning-your-raspberry-pi-zero-into-a-usb-gadget/ethernet-gadget
* https://developer.toradex.com/knowledge-base/how-to-install-microsoft-rndis-driver-for-windows-7
* https://desertbot.io/blog/ssh-into-pi-zero-over-usb
* https://core-electronics.com.au/tutorials/raspberry-pi-zerow-headless-wifi-setup.html
* http://wiringpi.com/download-and-install/