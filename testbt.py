"""This module captures images and sends via bluetooth"""
import sys

import bluetooth
from nOBEX.client import Client
from clickcam import Clicker

CONFIG = {
    "channel": 7
}


class BTSender():
    """send file to all available bt devices"""
    def __init__(self):
        self.__dev = bluetooth.discover_devices(lookup_names=True)
        print("found %d devices" % len(self.__DEV))

    def send(self, filename):
        """do the work"""
        for dev, name in self.__dev:
            print("sending to  %s (%s)" % (dev, name))
            _client = Client(dev, CONFIG["channel"])
            try:
                _client.connect()
                _client.put(filename, open(filename, 'rb').read())
                print("File send")
            except ConnectionRefusedError:
                print("Connection refused")


if __name__ == "__main__":
    try:
        CLICKER = Clicker()
        SENDER = BTSender()
    except SystemError as err:
        sys.exit()
    while True:
        CLICKER.capture()
        SENDER.send(CLICKER.filename)
        # CMDLINE = "obexftp --nopath --noconn -U none --channel %s -b %s -p \"%s\""
        # call(CMDLINE % (CONFIG["channel"], dev, CLICKER.filename), shell=True)
